package edu.ntnu.idatt2003;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DateUtilsTest {

    @Nested
    class AYearIsALeapYear {

        @Test
        public void yearIsDivisibleByFourButNotByOneHundred() {
            assertTrue(DateUtils.isLeapYear(2020));
        }

        @Test
        public void yearIsDivisibleByFourHundred() {
            assertTrue(DateUtils.isLeapYear(2000));
        }
    }

    @Nested
    class AYearIsNotALeapYear {

        @Test
        public void yearIsNotDivisibleByFour() {
            assertFalse(DateUtils.isLeapYear(1981));
        }

        @Test
        public void yearIsDivisibleByOneHundredButNotByFourHundred() {
            assertFalse(DateUtils.isLeapYear(2100));
        }
    }

    @Nested
    class AYearIsNotSupported {

        @Test
        public void yearIsNegative() {
            try {
                DateUtils.isLeapYear(-1);
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("Year must be >= 0", ex.getMessage());
            }
            // Or we could do this:
            // assertThrows(IllegalArgumentException.class, () -> DateUtils.isLeapYear(-1));
        }
    }
}
