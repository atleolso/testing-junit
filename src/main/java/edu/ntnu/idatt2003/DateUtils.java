package edu.ntnu.idatt2003;

public class DateUtils {

    public static boolean isLeapYear(int year) {
        if (year < 0) {
            throw new IllegalArgumentException("Year must be >= 0");
        }

        return (year % 4 == 0 && year % 100 != 0) ||
               (year % 400 == 0);
    }

}
